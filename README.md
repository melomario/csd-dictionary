# Random Line

This library returns a random line for a given file, but you must provide your
own file.

# Preparing your environment

* Install [Node and npm](https://https://nodejs.org/en/)
* Create an npm account here:
  [Node Package Manager](https://www.npmjs.com/signup)
* Go to [GitLab](http://gitlab.com) and create an account.

# Pushing your code there

* Create a new repository, and push this code there.
* run `npm login`
* run `npm init`
* Make sure to include your username on the package name. Example:
  `@melomario/random-line`

_A wild new file appears_, and it's called `package.json`.

# Writing your first test and function

Ok. It's time to learn how to manage our dependencies. You can run the following
command to add a test framework to your project:

`npm install --save-dev jest` or a shortcut: `npm i -D jest`

The `--save-dev` or `-D` section establishes that the jest framework is not
going to be necessary in production. We'll use it to validate our code, but we
don't need it bloating our production servers, right?

If you look into the `package.json` file, you'll see a new command. This command
is used to run tests: `npm run test`. Try it on your terminal.

# 💡 Exercise 1 💡

* There's a test with small problem. Find it and fix it.
* Also, try to add more words to the sample file `assets/words.txt` and run your
  tests again

# 💡 Exercise 2 💡

Now it's time to publish (the first version) of your library! Make sure you
insert some new words on your words file and then run `npm publish --access
public`
