const RandomWord = require("../lib/random_word");

describe("Nossa biblioteca de palavras aleatorias é capaz de", () => {
  test("ler um arquivo com palavras separadas por quebra de linha e me retornar uma lista dessas palavras", () => {

    expect( RandomWord.readWordsFile(RandomWord.WORD_FILE) ).toHaveLength(5);
  
  });

  test("receber uma lista de palavras e retornar uma palavra aleatória válida", () => {
    
    const words = ["bagagem", "adjudicar", "ribeira"];

    expect( RandomWord.getRandomWordFromFile(words) ).toBe("bagagem");
  
  });
});
