const files = require("fs");
const path = require("path");

const WORD_FILE = path.resolve(__dirname, "../assets/words.txt");

const getRandomWord = () => {
  let words_array = readWordsFile(WORD_FILE);
  return getRandomWordFromList(words_array);
};

const getRandomWordFromFile= words_array => {
  return words_array[0];
};

const readWordsFile = filename => {
  const words_array = files.readFileSync(filename, "utf-8").split("\n");
  return words_array;
};

module.exports = {
  WORD_FILE,
  readWordsFile,
  getRandomWordFromFile,
  getRandomWord
};
