const getRandomWord = require("./lib/random_word").getRandomWord;

module.exports = { getRandomWord };